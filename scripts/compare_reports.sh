#!/usr/bin/env sh

set -e

print_usage() {
  echo ""
  echo "USAGE:"
  echo "  ./compare_reports.sh [OPTIONS]"
  echo ""
  echo "EXAMPLE:"
  echo "  ./compare_reports.sh dast actual.json expected.json"
  echo ""
  echo "OPTIONS:"
  echo "  -h                                                           Display this help message."
  echo "  <report-type> <path-to-actual-file> <path-to-expected-file>  Compare reports. \`report-type\` must be one of:"
  echo "                                                                 - sast: Static Application Security Testing"
  echo "                                                                 - dast: Dynamic Application Security Testing"
  echo "                                                                 - cs:   Container Scanning"
  echo "                                                                 - lm:   License Management"
  echo "                                                                 - ls:   License Scanning"
  echo "                                                                 - cq:   Code Quality"
  echo "                                                                 - sd:   Secret Detection"
}

filter_for_report_type() {
  set -e

  local local_report_type jq_filter

  local_report_type=$1

  case "$local_report_type" in
    "lm" | "ls" | "cq")
      jq_filter="(.. | arrays) |= sort"
      ;;
    "dast")
      jq_filter='del(.["@generated", "@version"]) | (.. | arrays) |= sort'
      ;;
    "sast")
      jq_filter="del(.version) |
                       del(.scan.analyzer.version) |
                       del(.scan.scanner.version) |
                       del(.scan.start_time) |
                       del(.scan.end_time) |
                       del(.vulnerabilities[].id) |
                       del(.vulnerabilities[].location.dependency.iid) |
                       del(.dependency_files) |
                       del(.remediations[]?.fixes[].id) |
                       del(.vulnerabilities[]?.tracking) |
                       .vulnerabilities |= map_values(.links |= (. // [])) |
                       .vulnerabilities |= map_values(.identifiers |= (. // [])) |
                       .vulnerabilities[]?.tracking.items |= [] |
                       .vulnerabilities[]?.flags |= [] |
                       del(.dependency_files[]?.dependencies[].dependency_path[]?.iid) |
                       del(.scan.primary_identifiers) |
                       .vulnerabilities |= sort"
      ;;
    "sd")
      jq_filter="del(.version) |
                       del(.scan.analyzer.version) |
                       del(.scan.scanner.version) |
                       del(.scan.start_time) |
                       del(.scan.end_time) |
                       del(.vulnerabilities[]|.location.commit.sha) |
                       del(.vulnerabilities[].id) |
                       del(.remediations[]?.fixes[].id) |
                       del(.dependency_files) |
                       .vulnerabilities |= map_values(.links |= (. // [])) |
                       .vulnerabilities |= map_values(.identifiers |= (. // [])) |
                       .vulnerabilities |= sort"
      ;;
    *)
      echo >&2 "Error: Unknown report type '$local_report_type'. Please provide the" \
        "report type as the first argument.  Report type must be one of:" \
        "<sast|dast|cs|lm|ls|cq>"
      ;;
  esac

  echo "$jq_filter"
}

sanitize_report() {
  local error sanitized_output_file jq_filter report_name local_report_type

  local_report_type=$1
  report_name=$2

  jq_filter=$(filter_for_report_type "$local_report_type")

  # if jq_filter is empty, then it means that the user didn't provide a valid report_type
  if [ -z "$jq_filter" ]; then
    exit 1
  fi

  # the temporary file which contains the filtered/sorted/sanitized output will be named
  # '<report-name>-sanitized.json'
  sanitized_output_file=${report_name/.json/-sanitized.json}

  # disable "Exit immediately" behaviour so we can provide a more informative error message
  set +e
  error=$(/usr/bin/env jq -e "$jq_filter" "$report_name" 2>&1 >"$sanitized_output_file")
  set -e

  if [ "$error" ]; then
    rm "$sanitized_output_file"

    if [ "$error" != "${error%"cannot be sorted, as it is not an array"*}" ]; then
      echo >&2 "Error: jq encountered an error while attempting to parse the file:" \
        "'$report_name': '$error'. It appears as though the JSON file '$report_name'" \
        "consists of some objects which contain array elements, while other objects" \
        "are missing these elements. In order for jq to be able to sort the file, all" \
        "objects (entries) in the file must have the _same_ structure. Please ensure" \
        "that you've set the correct report-type value for the given report. If the report-type" \
        "has been set correctly, then you'll need to update the \`jq_filter\` variable" \
        "of this script to insert blank \`[]\` entries for these missing array elements" \
        "using the \`map_values\` function."
      exit 1
    fi

    echo >&2 "Error: jq encountered an error while attempting to parse the file '$report_name'. '$error'"
    exit 1
  fi

  echo "$sanitized_output_file"
}

parse_command_line_options() {
  set -e

  # Transform long options to short ones
  for arg in "$@"; do
    shift
    case "$arg" in
      "--help") set -- "$@" "-h" ;;
      *) set -- "$@" "$arg" ;;
    esac
  done

  # Parse short options
  OPTIND=1
  while getopts "h" opt; do
    case "${opt}" in
      "h")
        print_usage
        exit 0
        ;;
      "?")
        print_usage >&2
        exit 1
        ;;
    esac
  done
  shift $((OPTIND - 1)) # remove options from positional parameters

  report_type=$1
  actual_report=$2
  expected_report=$3

  if [ -z "$report_type" ]; then
    echo "Error: Please provide the type of the report."
    print_usage
    exit 1
  fi

  if [ -z "$actual_report" ]; then
    echo "Error: Please provide the path to the actual file."
    print_usage
    exit 1
  fi

  if [ ! -f "$actual_report" ]; then
    echo "Error: report with path '$actual_report' does not exist."
    print_usage
    exit 1
  fi

  if [ -z "$expected_report" ]; then
    echo "Error: Please provide the path to the expected file."
    print_usage
    exit 1
  fi

  if [ ! -f "$expected_report" ]; then
    echo "Error: report with path '$expected_report' does not exist."
    print_usage
    exit 1
  fi
}

# sort the reports, remove value fields that have variable contents, such as
# timestamps, and then compare the contents
sanitize_and_diff_reports() {
  set -e

  local sanitized_actual_report sanitized_expected_report diff_output
  local local_actual_report local_expected_report local_report_type

  local_report_type=$1
  local_actual_report=$2
  local_expected_report=$3

  sanitized_actual_report=$(sanitize_report "$local_report_type" "$local_actual_report")
  sanitized_expected_report=$(sanitize_report "$local_report_type" "$local_expected_report")

  set +e

  diff_output=$(diff -b -u "$sanitized_expected_report" "$sanitized_actual_report")
  diff_failed=$?

  rm "$sanitized_actual_report"
  rm "$sanitized_expected_report"

  if [ $diff_failed -ne 0 ]; then
    echo ""
    echo "Error occurred while comparing actual report '$local_actual_report'" \
      "against expected report '$local_expected_report': report contents differ:"
    echo ""
    echo "$diff_output"
    echo ""

    exit 1
  fi
}

# extracts the schema version from the given report and converts it to a git tag, which
# will be used for retrieving the matching schema from the secure report schema repository
#
# Example:
#
# input: { "version": "8.0.1", "vulnerabilities": [ ... ], "remediations": [], ... }
# output: v8.0.1-rc1
get_tag_name() {
  local local_actual_report schema_version tag_name schema_major_version

  local_actual_report=$1

  schema_version=$(/usr/bin/env jq -re ".version" "$local_actual_report")
  schema_major_version=$(echo "$schema_version" | cut -d. -f1)

  if [ "$schema_version" == "3.0.0" ]; then
    tag_name="v3.0.0"
  elif [ "$schema_major_version" -lt 10 ]; then
    tag_name="v$schema_version-rc1"
  else
    tag_name="v$schema_version"
  fi

  echo "$tag_name"
}

get_schema_file_name() {
  local local_report_type schema_file_name

  local_report_type=$1

  case "$local_report_type" in
    "cs")
      schema_file_name="container-scanning"
      ;;
    "dast")
      schema_file_name="dast"
      ;;
    "sast")
      schema_file_name="sast"
      ;;
    "sd")
      schema_file_name="secret-detection"
      ;;
  esac

  echo "$schema_file_name"
}

# we need to validate the actual report against the security report schemas.
# This is necessary because sanitizing the reports in the sanitize_and_diff_reports
# function will strip out fields that have variable contents such as the `start_time`
# field. This means that if a bug occurred and the actual report was missing this
# `start_time` field, it would not cause the test to fail, which is the opposite of
# what we want. By validating the actual report against the schema, we can ensure that
# no required fields are missing.
validate_report_json() {
  local local_report_type local_security_report_schema_directory_url security_report_schema_url
  local local_actual_report validation_response json_schema_validation_failed security_report_schema_file

  local_report_type=$1
  local_security_report_schema_directory_url=$2
  local_actual_report=$3

  schema_file_name=$(get_schema_file_name "$local_report_type")
  tag_name=$(get_tag_name "$local_actual_report")

  # replace the TAGNAME placeholder value with the actual git tag of the schema
  security_report_schema_url=${local_security_report_schema_directory_url/TAGNAME/$tag_name}/${schema_file_name}-report-format.json

  security_report_schema_file="$local_security_report_schemas_dir/${schema_file_name}-report-format.json"

  echo >&2 ""
  echo >&2 "Fetching Security Report Schema from '$security_report_schema_url'"
  echo >&2 ""
  wget "$security_report_schema_url" -O "$security_report_schema_file"
  json_schema_fetch_failed=$?

  if [ $json_schema_fetch_failed -ne 0 ]; then
    echo ""
    echo "Error occurred while fetching JSON schema for report '$local_actual_report'"
    echo ""

    # See https://gitlab.com/gitlab-org/gitlab/-/issues/299866 for details
    exit 65
  fi

  validation_response=$("$json_schema_validator" --instance "$local_actual_report" "$security_report_schema_file" 2>&1)
  json_schema_validation_failed=$?

  rm "$security_report_schema_file"

  if [ $json_schema_validation_failed -ne 0 ]; then
    echo ""
    echo "Error occurred while validating report '$local_actual_report'" \
      "against JSON schema '$security_report_schema_url':"
    echo "$validation_response"
    echo ""

    # See https://gitlab.com/gitlab-org/gitlab/-/issues/299866 for details
    exit 65
  fi
}

# Global variables
report_type=""
actual_report=""
expected_report=""
json_schema_validator="jsonschema"
security_report_schema_directory_url="https://gitlab.com/alekitto-gitlab-org-forks/security-products/security-report-schemas/-/raw/TAGNAME/dist"
local_security_report_schemas_dir="/tmp/security_report_schemas"

mkdir -p "$local_security_report_schemas_dir"

parse_command_line_options "$@"

sanitize_and_diff_reports "$report_type" "$actual_report" "$expected_report"

if [ "$report_type" != "ls" ]; then
  validate_report_json "$report_type" "$security_report_schema_directory_url" "$actual_report"
fi
