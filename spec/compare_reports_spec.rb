describe "CompareReports" do
  context 'sast' do
    let(:expected) { "test/expect/gl-sast-report.json" }

    context "when the report contents differ" do
      let(:actual) { "test/fixtures/gl-sast-report-with-incorrect-name.json" }

      it "outputs an error explaining that the report contents differ" do
        expect { system %(./scripts/compare_reports.sh sast #{actual} #{expected}) }
          .to output(a_string_including(%(Error occurred while comparing actual report '#{actual}' against expected report '#{expected}': report contents differ))).to_stdout_from_any_process
      end
    end

    context "when the report is missing a field" do
      context "and the schema version for the given report requires the field to be present" do
        let(:actual) { "test/fixtures/gl-sast-report-without-messages-field-version-7.0.1.json" }

        it "outputs an error explaining that the report contents differ" do
          expect { system %(./scripts/compare_reports.sh sast #{actual} #{expected}) }
            .to output(a_string_including(%('messages' is a required property))).to_stdout_from_any_process
        end
      end

      context "and the tracking field is missing" do
        let(:actual) { "test/fixtures/gl-sast-report-with-tracking.json" }

        it "outputs an error explaining that the report contents differ" do
          expect { system %(./scripts/compare_reports.sh sast #{actual} #{expected}) }
          .to output(a_string_including(%(Error occurred while comparing actual report '#{actual}' against expected report '#{expected}': report contents differ))).to_stdout_from_any_process
        end
      end

      context "and the schema version for the given report does not require the field to be present" do
        let(:actual) { "test/fixtures/gl-sast-report-without-messages-field-version-8.0.0.json" }

        it "does not output any errors" do
          expect { system %(./scripts/compare_reports.sh sast #{actual} #{expected}) }
            .to output("").to_stdout_from_any_process
        end
      end
    end

    context "when the report and expected report are both missing a required 'status' field" do
      let(:actual) { "test/fixtures/gl-sast-report-without-status-field.json" }
      let(:expected) { actual }

      it "outputs an error explaining that the 'status' field is missing" do
        expect { system %(./scripts/compare_reports.sh sast #{actual} #{expected}) }
          .to output(a_string_including(%('status' is a required property))).to_stdout_from_any_process
      end
    end

    context "when the report does not include 'remediations' array" do
      let(:actual) { "test/fixtures/gl-sast-report-without-remediations-array.json" }
      let(:expected) { actual }
      it "does not output any errors" do
        expect { system %(./scripts/compare_reports.sh sast #{actual} #{expected}) }
          .to output("").to_stdout_from_any_process
      end
    end
  end
end
