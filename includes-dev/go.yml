variables:
  GO_VERSION: "1.19"

.go:
  image: golang:$GO_VERSION
  stage: test

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml

go test:
  extends: .go
  script:
    - go get -t ./...
    - go install gotest.tools/gotestsum@latest
    - go install github.com/boumenot/gocover-cobertura@latest
    # run tests, create coverprofile, and create junit report
    - gotestsum --junitfile report.xml --format testname -- -race -coverprofile=coverage.txt -covermode atomic ./...
    # write coverage report
    - gocover-cobertura < coverage.txt > coverage.xml
    # extract coverage score
    - go tool cover -func coverage.txt
  coverage: '/total:.*\d+.\d+%/'
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: report.xml

go lint:
  extends: .go
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  script:
    - wget https://gitlab.com/gitlab-org/security-products/ci-templates/-/raw/master/.golangci.yml
    - golangci-lint run --verbose --out-format code-climate:codequality.json,colored-line-number
  artifacts:
    when: always
    expire_in: 1 month
    reports:
      codequality: codequality.json

goimports:
  extends: .go
  script:
    - go install golang.org/x/tools/cmd/goimports@latest
    - FMT_CMD='goimports -w -local gitlab.com/gitlab-org .'
    - eval "$FMT_CMD"
    - |
      if ! git diff --ignore-submodules=dirty --exit-code; then
        echo
        echo "Some files are not formatted. Please format with \`$FMT_CMD\`"
        echo 'See https://docs.gitlab.com/ee/development/go_guide/#code-style-and-format-1 for more information'
        exit 1
      fi

go mod tidy:
  extends: .go
  script:
    - wget https://gitlab.com/gitlab-org/security-products/ci-templates/-/raw/master/.lefthook/pre-push/go-mod-tidy
    - sh go-mod-tidy

danger-review:
  stage: test
  image: registry.gitlab.com/gitlab-org/security-products/danger-bot:$DANGER_BOT_VERSION
  needs: []
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/'
      when: never
    - if: $DANGER_DISABLED
      when: never
    - if: '$CI_COMMIT_BRANCH && $CI_PROJECT_NAMESPACE == "gitlab-org/security-products/analyzers"'
    - when: never
  variables:
    DANGER_BOT_VERSION: v0.12
  script:
    - cp -r /danger/ danger/
    - mv danger/analyzers/Dangerfile Dangerfile
    - |
      if [ -f danger/Gemfile ]; then
        mv danger/Gemfile Gemfile
        bundle config set path danger/vendor/
        bundle exec danger --fail-on-errors=true
      else
        danger --fail-on-errors=true
      fi
