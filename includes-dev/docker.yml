variables:
  # SETTINGS:
  #
  # path of the official images, relative to CI_REGISTRY_IMAGE;
  # it's empty by default, and official images are published to CI_REGISTRY_IMAGE
  IMAGE_PATH: ""

  # path of the temporary image, relative to CI_REGISTRY_IMAGE
  TMP_IMAGE_PATH: "/tmp"

  # SHARED VARIABLES:
  #
  # name of the official image, without the image tag;
  # the IMAGE_TAG is set in the CI jobs of the release stage
  IMAGE_NAME: "$CI_REGISTRY_IMAGE$IMAGE_PATH"

  # name of the temporary image, including the tag;
  # the image tag is always the SHA1 of the git commit
  TMP_IMAGE: "$CI_REGISTRY_IMAGE$TMP_IMAGE_PATH:$CI_COMMIT_SHA"

  # optional suffix added to the image tags, like "-fips";
  # this suffix is used in both temporary images and official images
  IMAGE_TAG_SUFFIX: ""

stages:
  - build-image
  - tag
  - release-version
  - release-major

build tmp image:
  image: docker:20.10
  stage: build-image
  variables:
    DOCKERFILE: "Dockerfile"
  services:
    - docker:20.10-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg GO_VERSION -t $TMP_IMAGE$IMAGE_TAG_SUFFIX -f $DOCKERFILE .
    - docker push $TMP_IMAGE$IMAGE_TAG_SUFFIX

build tmp image fips:
  extends: build tmp image
  variables:
    IMAGE_TAG_SUFFIX: "-fips"
    DOCKERFILE: "Dockerfile.fips"
  rules:
    - exists:
      - 'Dockerfile.fips'

.docker_tag:
  image: alekitto/docker:latest
  stage: release-version
  services:
    - docker:20.10-dind
  variables:
    DOCKERFILE: "Dockerfile"
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export TARGET_IMAGE=$CI_REGISTRY_IMAGE:${IMAGE_TAG:-$CI_JOB_NAME}$IMAGE_TAG_SUFFIX
    - docker buildx create --use --bootstrap
    - docker buildx build --platform linux/amd64,linux/arm64 --build-arg GO_VERSION -t $TARGET_IMAGE -f $DOCKERFILE . --push
    - |
      if [[ -n "$SEC_REGISTRY_IMAGE" && \
            -n "$SEC_REGISTRY_PASSWORD" && \
            -n "$SEC_REGISTRY_USER" ]]
      then
        SEC_REGISTRY=${SEC_REGISTRY_IMAGE%%/*}
        docker login -u $SEC_REGISTRY_USER -p $SEC_REGISTRY_PASSWORD $SEC_REGISTRY
        TARGET_IMAGE=$SEC_REGISTRY_IMAGE:$IMAGE_TAG$IMAGE_TAG_SUFFIX
        docker buildx build --platform linux/amd64,linux/arm64 --build-arg GO_VERSION -t $TARGET_IMAGE -f $DOCKERFILE . --push
      fi

tag branch:
  extends: .docker_tag
  variables:
    # CAUTION: by preferring `SLUG` over `NAME` we can properly handle non-alphanumeric
    # characters, but this may limit our tags to 63chars or raise potential conflicts.
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/'
      when: never
    - if: $CI_COMMIT_BRANCH

tag branch fips:
  extends: tag branch
  variables:
    IMAGE_TAG_SUFFIX: "-fips"
    DOCKERFILE: "Dockerfile.fips"
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/'
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'Dockerfile.fips'

tag edge:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/'

tag edge fips:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge-fips
    DOCKERFILE: "Dockerfile.fips"
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/'

# Script that sets RELEASE, RELEASE_MAJOR, and RELEASE_MINOR.
# It uses the highest git tag if PUBLISH_IMAGES is set,
# or else it uses CI_COMMIT_TAG.
.set_release_vars_script: &set_release_vars_script
    - |
      if [[ -n "$PUBLISH_IMAGES" ]]; then
        # Install git and node-semver in order to list and sort git tags
        apk add --no-cache git nodejs npm
        npm install -g semver
        # Get git tags that match the MAJOR version, sort them as SemVer versions, and keep the last version
        CHANGELOG_MAJOR_VERSION=$(grep -m 1 '^## v' CHANGELOG.md | sed -n "s/^## v\(\d\+\)\..*$/\1/p")
        LAST_VERSION=$(semver $(git tag -l|grep -e v$CHANGELOG_MAJOR_VERSION'\.\d\+\.\d\+')|tail -n 1)
        # SemVer normalizes the version and strips off the leading "v", so we need to prepend "v" to the SemVer version to get the git tag
        CI_COMMIT_TAG="v$LAST_VERSION"
        echo "re-building image for highest git tag ${CI_COMMIT_TAG}"
      else
        echo "building new image for git tag ${CI_COMMIT_TAG}"
      fi
    # Drop SemVer build tag
    - export RELEASE=${CI_COMMIT_TAG/+*/}
    - echo "release ${RELEASE}"
    # Cut off last dot and whatever follows it
    - export MINOR_RELEASE=${RELEASE%.*}
    - echo "minor release ${MINOR_RELEASE}"
    # Cut off two dots and whatever follows it
    - export MAJOR_RELEASE=${RELEASE%.*.*}
    - echo "major release ${MAJOR_RELEASE}"
    # Checking consistency b/w git tag and changelog
    - echo "Checking that $RELEASE is last in the changelog"
    - |
      if [ ! "$(grep -m 1 '^## v' CHANGELOG.md)" = "## $RELEASE" ]; then
        echo "Expected CHANGELOG.md version: $(grep -m 1 '^## v' CHANGELOG.md | cut -d ' ' -f 2) to equal RELEASE variable: $RELEASE"
        exit 1
      fi

.release:
  extends: .docker_tag
  stage: release-major
  rules:
    - if: $CI_COMMIT_TAG
    - if: '($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/) && $PUBLISH_IMAGES'

.release-fips:
  extends: .release
  variables:
    IMAGE_TAG_SUFFIX: "-fips"
    DOCKERFILE: 'Dockerfile.fips'
  rules:
    - if: $CI_COMMIT_TAG
      exists:
        - 'Dockerfile.fips'
    - if: '($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/) && $PUBLISH_IMAGES'
      exists:
        - 'Dockerfile.fips'

release major:
  extends: .release
  before_script:
    - *set_release_vars_script
    # Drop leading v
    - export IMAGE_TAG=${MAJOR_RELEASE/v/}

release major fips:
  extends: .release-fips
  before_script:
    - *set_release_vars_script
    # Drop leading v
    - export IMAGE_TAG=${MAJOR_RELEASE/v/}

release minor:
  extends: .release
  before_script:
    - *set_release_vars_script
    # Drop leading v
    - export IMAGE_TAG=${MINOR_RELEASE/v/}

release minor fips:
  extends: .release-fips
  before_script:
    - *set_release_vars_script
    # Drop leading v
    - export IMAGE_TAG=${MINOR_RELEASE/v/}

release patch:
  extends: .release
  before_script:
    - *set_release_vars_script
    # Drop leading v
    - export IMAGE_TAG=${RELEASE/v/}

release patch fips:
  extends: .release-fips
  before_script:
    - *set_release_vars_script
    # Drop leading v
    - export IMAGE_TAG=${RELEASE/v/}

release latest:
  extends: .release
  variables:
    IMAGE_TAG: latest
